package com.sun.order.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sun.commons.pojo.TbItemVO;
import com.sun.order.pojo.CreateOrderVO;

public interface OrderService {

	List<TbItemVO> catList(HttpServletRequest request,Long[] id_array);
	
	/**
	 * 创建订单
	 * @param createOrderVO
	 * @return
	 */
	boolean createOrder(HttpServletRequest request,CreateOrderVO createOrderVO)throws Exception;
	
}
