package com.sun.search.service;

import java.util.List;
import java.util.Map;

import com.sun.commons.pojo.SolrInsertData;

/**
 * 同步数据到搜索引擎
 * @author Sunhongmin
 *
 */
public interface SolrSynService {

	/**
	 * 初始同步或者外源数据导入后同步
	 * @Param is_all 是否全部更新  此选项为true 则更新全部商品   如果为false 则以上次更新的时间为起点更新到最新数据
	 * @return
	 */
	Map<String,Object> initData(boolean is_all);
	
	/**
	 * 单条商品信息同步
	 * @return
	 */
	boolean insertData(SolrInsertData solrInsertData);
	
	/**
	 * 单条商品信息更新索引
	 * @return
	 */
	boolean updateData(SolrInsertData solrInsertData);
	
	/**
	 * 单条商品信息删除  同步索引
	 * @return
	 */
	boolean deleteDate(List<String> ids);
}
