/**
 * 全局url地址
 * 调用：       url=GLOBALURL.webmanage_base_address+"/add/info"
 * 8080     GLOBALURL.webmanage_base_address
   8081   	GLOBALURL.item_base_address
   8082	    GLOBALURL.portal_base_address
   8083	    GLOBALURL.search_base_address
   8084	    GLOBALURL.passport_base_address
   8085	    GLOBALURL.cart_base_address
   8086	    GLOBALURL.order_base_address
 * */
var GLOBALURL=(function(){
	
	var local_webmanage = 'http://localhost:8080/webmanage';
	var local_item = 'http://localhost:8081';
	var local_portal = 'http://localhost:8082';
	var local_search = 'http://localhost:8083';
	var local_passport = 'http://localhost:8084';
	var local_cart = 'http://localhost:8085';
	var local_order = 'http://localhost:8086';
	
	
	var webmanage = 'http://manager.eshop.com:8080/webmanage';
	var item = 'http://item.eshop.com';
	var portal = 'http://www.eshop.com';
	var search = 'http://search.eshop.com';
	var passport = 'http://passport.eshop.com';
	var cart = 'http://cart.eshop.com';
	var order = 'http://order.eshop.com';
	
	//本地
	return {
		webmanage_base_address:local_webmanage
		,item_base_address:local_item
		,portal_base_address:local_portal
		,search_base_address:local_search
		,passport_base_address:local_passport
		,cart_base_address:local_cart
		,order_base_address:local_order
	}
	
	//服务器
	/*return {
		webmanage_base_address:webmanage
		,item_base_address:item
		,portal_base_address:portal
		,search_base_address:search
		,passport_base_address:passport
		,cart_base_address:cart
		,order_base_address:order
	}*/
	
})();