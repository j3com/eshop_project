package com.sun.item.controller;

import javax.annotation.Resource;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.item.service.ItemMenuService;
@Controller
public class ItemMenuController {

	@Resource
	private ItemMenuService itemMenuService;
	
	@ResponseBody
	@RequestMapping("/rest/itemcat/all")
	public MappingJacksonValue listPortalCatMenu(String callback){
		MappingJacksonValue value = new MappingJacksonValue(itemMenuService.listAllCatMenu());
		value.setJsonpFunction(callback);
		return value;
	}
}
